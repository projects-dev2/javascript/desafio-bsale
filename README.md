<h1>
  <a href="https://desafio-begazo-carhuayo.netlify.app/">
    Desafío Bsale
  </a>
</h1>


Desarrollo del Desafío Bsale, el cual consiste una tienda online, que despliegue:
 - Productos agrupados por la categorpia que pertenecen
 - Un buscador de productos que sigue el patron que el usuario ingresa en el input de busqueda (en el lado del servidor nodejs)
 - Cuenta con filtros de categorías y/o precio ascendente o descendente. 
 - Como también con un paginador creado desde cero, pero apoyado con estilos css del CDN Bootstrap, el CDN Javascript de Bootstrap solo se agregó para el responsive del navbar.

## Repositorios

- Repositorio Back:   https://gitlab.com/projects-dev2/javascript/desafio-backend
- Repositorio Front:  https://gitlab.com/projects-dev2/javascript/desafio-front


## Autores ✒️

_Mencion a quienes ayudaron a levantar el proyecto desde sus inicios_

* **Begazo Carhuayo Alvaro Martín** - *Trabajo Inicial* - [AlvaroB15](https://gitlab.com/projects-dev2/javascript)
